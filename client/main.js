// Libs

import {Meteor} from "meteor/meteor";
import Vue from "vue";
import routerFactory from '/client/configs/router.config'
import '/client/configs/vuetify.config'
// Main app
import AppComponent from "/imports/ui/App.vue";

Meteor.startup(() => {
  const router = routerFactory.create()

  new Vue({
    router,
    render: h => h(AppComponent)
  }).$mount('app')
});