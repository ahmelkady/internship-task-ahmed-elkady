import { Mongo } from 'meteor/mongo'
export const Requests= new Mongo.Collection('requests')
export const Orders = new Mongo.Collection('orders')
export const Ratings = new Mongo.Collection('ratings')

